//
//  SceneDelegate.h
//  PictureSocketClient
//
//  Created by 吴家行 on 2020/7/24.
//  Copyright © 2020 Jiahang Wu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

