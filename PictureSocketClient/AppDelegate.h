//
//  AppDelegate.h
//  PictureSocketClient
//
//  Created by 吴家行 on 2020/7/24.
//  Copyright © 2020 Jiahang Wu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

