//
//  ViewController.h
//  PictureSocketClient
//
//  Created by 吴家行 on 2020/7/24.
//  Copyright © 2020 Jiahang Wu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    IBOutlet UIButton* button;
    IBOutlet UITextView* text_view;
    IBOutlet UILabel* imgNameLabel;
    __weak IBOutlet UIStepper *imgNameStepper;
}
- (IBAction)startButton:(id)sender;
- (IBAction)pressStepper:(id)sender;
@end

