//
//  ViewController.m
//  PictureSocketClient
//
//  Created by 吴家行 on 2020/7/24.
//  Copyright © 2020 Jiahang Wu. All rights reserved.
//


#import "ViewController.h"
#include <filesystem>
#include <fstream>
#include <arpa/inet.h>
#include <sys/types.h>
#include <unistd.h>
#include <sstream>
#include <string>
#import <vector>
#include <time.h>
#import <CoreTelephony/CTCellularData.h>
using namespace std::__fs::filesystem;
using namespace std;
@interface ViewController ()
{
    int sock;
    vector<string> img_lists;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // 假设在程序没有联网权限的时候，去（使用网络）连接一下百度
    NSURL *url = [NSURL URLWithString:@"http://www.baidu.com"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {}];
    
    // 联网权限获取
    CTCellularData *cellularData = [[CTCellularData alloc] init];
    cellularData.cellularDataRestrictionDidUpdateNotifier = ^(CTCellularDataRestrictedState state){
        //获取联网状态
        switch (state) {
            case kCTCellularDataRestricted:
                NSLog(@"Restricrted");
                break;
            case kCTCellularDataNotRestricted:
                NSLog(@"Not Restricted");
                break;
            case kCTCellularDataRestrictedStateUnknown:
                NSLog(@"Unknown");
                break;
            default:
                break;
        };
    };
    NSString *documentPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    auto img_root = path([documentPath UTF8String]);
    std::string img_path = img_root / ("imgs");
    for (const auto & entry : directory_iterator(img_path)){
        string jpg = "jpg";
        auto img_name = entry.path().filename().string();
        if(img_name.compare(img_name.size() - jpg.size(), jpg.size(), jpg) == 0){
            img_lists.push_back(entry.path().filename().string());
        }
        printf("img: %s\n", entry.path().filename().c_str());
    }
    imgNameStepper.maximumValue = img_lists.size() - 1;
    imgNameStepper.value = 12;
    string current_img_str = img_lists[int(imgNameStepper.value)];
    NSString * currentImg = [NSString stringWithCString: current_img_str.c_str()
                                                 encoding:[NSString defaultCStringEncoding]];
    imgNameLabel.text = currentImg;
    [text_view setEditable:NO];
    
    
}

- (void)startButton:(id)sender{
    
    const std::string ip_addr ="192.168.31.71";
    const std::string port = "11332";
    
    NSString *documentPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    auto jpg_root = path([documentPath UTF8String]);
    string current_img_str = img_lists[int(imgNameStepper.value)];
    const std::string jpg_file = jpg_root / ("imgs") / current_img_str;
    FILE *jpg_handler = fopen(jpg_file.data(), "rb");
    FILE* file = fopen(jpg_file.data(), "rb");
    // 如何获取一个文件的data_len？？？
    int data_len = 0;
    if (file) {
        fseek(file,0,SEEK_END);     //定位到文件末
        data_len = (int)ftell(file);       //文件长度
        printf("data_len: %d\n", data_len);
        fclose(file);
    }else{
        fprintf(stderr, "Open file Failed \n");
    }
    int i = 3;
    vector<int32_t> send_times;
    vector<int32_t> recv_times;
    vector<int32_t> total_times;
    for(int i = 0; i < 3; i++){
        struct sockaddr_in server_addr{};
        if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            fprintf(stderr, "Socket creation error \n");
        }
        server_addr.sin_family = AF_INET;
        int port_num = std::stoi(port);
        server_addr.sin_port = htons(port_num);
        // Convert IPv4 and IPv6 addresses from text to binary form
        if (inet_pton(AF_INET, ip_addr.data(), &server_addr.sin_addr) <= 0) {
            fprintf(stderr, "Address not supported \n");
        }
        if (connect(sock, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
            fprintf(stderr, "Connection Failed \n");
        }
        fprintf(stdout, "Connect to server successfully!\n");
        
        
        
        
        uint8_t *data = (uint8_t *) malloc(sizeof(uint8_t) * (data_len + 1));
        fread(data, sizeof(uint8_t), data_len, jpg_handler);
        
        int32_t header[2];
        header[0] = data_len;
        time_t myt = time(NULL);
        header[1] = (int32_t)myt;
        printf("start_time:%dms\n",header[1]);
        send(sock, header, sizeof(int32_t)*2, 0);
        int send_n = 0;
        int len = header[0];
        
        while (send_n < header[0]) {
            long n = send(sock, data + send_n, len - send_n, 0);
            if (n < 0) {
                fprintf(stderr, "socket() failed: %s\n", strerror(errno));
                break;
            } else {
                send_n += n;
            }
        }
        int32_t reply[2];
        recv(sock, &reply, sizeof(int32_t) * 2, 0);
    //    printf("%d %d", reply[0], reply[1]);
        int32_t time_send_from_client = reply[1];
        int32_t time_recv_on_server = reply[0];
        int32_t time_recv_on_client = (int32_t)time(NULL);
        send_times.push_back(time_recv_on_server-time_send_from_client);
        recv_times.push_back(time_recv_on_client-time_recv_on_server);
        total_times.push_back(time_recv_on_client-time_send_from_client);
    }
    float ave_send_time, ave_recv_time, ave_total_time;
    long long sum = 0;
    for(int i = 0; i < send_times.size(); i++)
        sum += send_times[i];
    ave_send_time = sum * 1.0/ send_times.size();
    sum = 0;
    for(int i = 0; i < recv_times.size(); i++)
        sum += recv_times[i];
    ave_recv_time = sum * 1.0/ recv_times.size();
    sum = 0;
    for(int i = 0; i < total_times.size(); i++)
        sum += total_times[i];
    ave_total_time = sum * 1.0/ total_times.size();
    NSString *str = text_view.text;
    text_view.text = [str stringByAppendingString:[NSString stringWithFormat:
                                                   @"image_size: %dByte\nsend img avg time cost: %fms\nrecv img avg time cost: %fms\navg total cost: %fms\n=========\n",data_len,
                                                   ave_send_time , ave_recv_time, ave_total_time]];
        
}
- (IBAction)pressStepper:(id)sender {
    
    string current_img_str = img_lists[int(imgNameStepper.value)];
    NSString * currentImg = [NSString stringWithCString: current_img_str.c_str()
                                                 encoding:[NSString defaultCStringEncoding]];
    imgNameLabel.text = currentImg;
}
@end
