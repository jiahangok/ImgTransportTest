from enum import Enum
import time
import struct
from io import StringIO, BytesIO
import numpy as np
import json
import socket
import logging
import threading



class ServerSocket:
    def __init__(self, ip, port):
        super().__init__()
        self.ip = ip
        self.port = port
        self.client_socket = None
        self.server_socket = None

    def recv(self):
        # receive the length of the message
        try:
            msg_size = 4*2
            recv_buffer = b''
            while len(recv_buffer) < msg_size:
                sock_data = self.client_socket.recv(msg_size - len(recv_buffer))
                recv_buffer = recv_buffer + sock_data
            # receive the message
            file_size = struct.unpack('=2i', recv_buffer)[0]
            start_time = struct.unpack('=2i', recv_buffer)[1]
            print("file_size:{}".format(file_size))
            print("client send time: {} ms".format(start_time))
            recv_buffer = b''
            new_filename = 'img.jpg'
            fp = open(new_filename, 'wb')
            while len(recv_buffer) < file_size:
                sock_data = self.client_socket.recv(file_size - len(recv_buffer))
                recv_buffer = recv_buffer + sock_data
                fp.write(sock_data)  # 写入图片数据
            fp.close()
            msg = start_time
        except Exception as e:
            print("recv message error")
            print(e.args)
            return None
        return msg

    def send(self, msg):
        status = True
        print(msg)
        try:
            msg_data = msg
            # print(len(msg_data))
            self.client_socket.send(struct.pack('=2i', *msg_data))
        except Exception as e:
            print("send message error")
            print(e.args)
            status = False
        return status
    
    def start(self):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind(('', self.port))
        print('start listen to {}'.format(self.port))
        self.server_socket.listen(1)
        self.client_socket, client_address = self.server_socket.accept()
        print("{0} connect to the server".format(client_address))

if __name__ == "__main__":
    server_socket = ServerSocket("", 11332)
    
    while True:
        server_socket.start()
        client_start_time = server_socket.recv()

        msg = []
        msg.append(int(time.time()))
        msg.append(int(client_start_time))
        print('server recv time: {} ms'.format(msg))
        server_socket.send(msg)
        # print(msg)